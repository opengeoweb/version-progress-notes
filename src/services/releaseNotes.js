import axios from "axios";
const releaseUrl = "https://gitlab.com/api/v4/projects/21149814/releases?per_page=100";

const getReleases = () => {
    const request = axios.get(releaseUrl)
    return request.then(response => response.data)
}

const getReleaseNotes = (id) => {
    const request = axios.get(`https://gitlab.com/api/v4/projects/17465079/issues?labels=Release::${id}`)
    return request.then(response => response.data)
}

export default {
    getReleases, getReleaseNotes
}