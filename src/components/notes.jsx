import { useEffect, useState } from 'react'
import NoteItem from './noteItem'

import releaseService from '../services/releaseNotes'

const VersionDetails = ({name, rDate}) => {
    const [releaseNotes, setReleaseNotes] = useState([])
    useEffect(() => {
        releaseService.getReleaseNotes(name).then(releaseNotes => {
            setReleaseNotes(releaseNotes)
        })
    }, [])
    
    if (releaseNotes.length > 0) {
        return (
            <div>
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-end'}}>
                    <h2 id={name} style={{marginBottom: 0}}>{name}</h2>
                    <h4 style={{color: 'gray', marginLeft: '1rem', marginBottom: 0}}> Released {rDate ? rDate.slice(0, 10) : ""} </h4>
                </div>
                    {releaseNotes.some(note => note.labels.includes('USER STORY') && note.state === 'closed') && (
                        <>
                            <h3>New Features (closed):</h3>
                            <ul>{releaseNotes.filter(note => (note.labels.includes('USER STORY') && note.state === 'closed')).map(note => <NoteItem key={note.iid} note={note}/>)}</ul>
                        </>
                    )}
                    {releaseNotes.some(note => note.labels.includes('USER STORY') && note.state === 'opened') && (
                        <>
                            <h3>New Features (still to be validated/tested):</h3>
                            <ul>{releaseNotes.filter(note => (note.labels.includes('USER STORY') && note.state === 'opened')).map(note => <NoteItem key={note.iid} note={note}/>)}</ul>
                        </>
                    )}
                    {releaseNotes.some(note => note.labels.includes('BUG') && note.state === 'closed') && (
                        <>
                            <h3>Solved Bugs (closed):</h3>
                            <ul>{releaseNotes.filter(note => (note.labels.includes('BUG') && note.state === 'closed')).map(note => <NoteItem key={note.iid} note={note}/>)}</ul>
                        </>
                    )}
                    {releaseNotes.some(note => note.labels.includes('BUG') && note.state === 'opened') && (
                        <>
                            <h3>Solved Bugs (still to be validated/tested):</h3>
                            <ul>{releaseNotes.filter(note => (note.labels.includes('BUG') && note.state === 'opened')).map(note => <NoteItem key={note.iid} note={note}/>)}</ul>
                        </>
                    )}
                    {releaseNotes.some(note => note.labels.includes('TECH DEBT') && note.state === 'closed') && (
                        <>
                            <h3>Fixed Technical Debt (closed):</h3>
                            <ul>{releaseNotes.filter(note => (note.labels.includes('TECH DEBT') && note.state === 'closed')).map(note => <NoteItem key={note.iid} note={note}/>)}</ul>
                        </>
                    )}
                    <hr/>
            </div>
        )
    }
}

export default VersionDetails