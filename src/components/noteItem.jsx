const NoteItem = ({note}) => {
    let textKNMI = note.labels.includes('👨🏻‍🦲 PO KNMI 🇳🇱') ? ' [KNMI only] ' : ''
    let textFMI = note.labels.includes('👩 PO FMI 🇫🇮') ? ' [FMI only] ' : ''
    let textMetNO = note.labels.includes('👱‍♀️ PO MetNO 🇳🇴') ? ' [MetNO only] ' : ''
    return (
        <li key={note.iid}>{note.title} {}
        [<a href={`https://gitlab.com/opengeoweb/geoweb-assets/-/issues/${note.iid}`}>{`#`+ note.iid}</a>] {}
        [<i>{note.epic?.title ?? ''}</i>]
        {textKNMI} {textFMI} {textMetNO}</li>
    )
}
export default NoteItem
