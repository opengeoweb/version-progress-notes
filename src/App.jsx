import { useState, useEffect } from 'react'
import './App.css'

import releaseService from './services/releaseNotes'
import VersionDetails from './components/notes'

function App() {
  const [count, setCount] = useState(0)
  const [releases, setReleases] = useState([])

  useEffect(() => {
    releaseService.getReleases().then(releases => {
      setReleases(releases)
    })
  }, [])

  return (
    <>
      <div>
        <h1>Open GeoWeb ACC Version Progress Notes</h1>
          {releases.map(release => <VersionDetails key={release.tag_name} name={release.tag_name} rDate={release.released_at}/>)}
      </div>
    </>
  )
}

export default App
