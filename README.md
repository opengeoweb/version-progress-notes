# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh


## Installation

Install [node](https://nodejs.org/en) and execute `npm install` in the project root.

## Development

Run command `npm run dev`. By default, Vite uses the [local port 5173](http://localhost:5173/).

## Build

Run command `npm run build`. This produces a static build ready to be hosted, e.g., in [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).